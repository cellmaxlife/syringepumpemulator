﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace SyringPumpSimulator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Launched Syringe Pump Simulator");
            SerialPortReplier simulator = new SerialPortReplier();
            double infuserate = 0.0;
            double refillrate = 0.0;
            double infusestartrate = 0.0;
            double refillstartrate = 0.0;
            double infuseendrate = 0.0;
            double refillendrate = 0.0;
            double infuseramptime = 0.0;
            double refillramptime = 0.0;

            try
            {
                simulator.OpenPort();
                while (true)
                {
                    string command = simulator.GetCommand();
                    Console.WriteLine("Command = " + command);
                    string reply = "";
                    if (command.Equals("cmd"))
                    {
                        reply = "Ultra\n";
                    }
                    else if (command.Equals("mode"))
                    {
                        reply = "Quick Start - \n";
                    }
                    else if (command.Equals("poll"))
                    {
                        reply = "Polling mode is REMOTE\n";
                    }
                    else if (command.Equals("irate"))
                    {
                        reply = "irate = " + infuserate.ToString("F2") + " ml/min\n";
                    }
                    else if (command.IndexOf("irate") > -1)
                    {
                        reply = ">\n";
                        infuserate = getnumber(1, command);
                    }
                    else if (command.Equals("wrate"))
                    {
                        reply = refillrate.ToString("F2") + " ml/min\n";
                    }
                    else if (command.IndexOf("wrate") > -1)
                    {
                        reply = ">\n";
                        refillrate = getnumber(1, command);
                    }
                    else if (command.Equals("iramp"))
                    {
                        reply = "iramp=" + infusestartrate.ToString("F2") + ", " + infuseendrate.ToString("F2") + " ml/min " + infuseramptime.ToString("F2") + " sec\n";
                    }
                    else if (command.IndexOf("iramp") > -1)
                    {
                        reply = ">\n";
                        infusestartrate = getnumber(1, command);
                        infuseendrate = getnumber(3, command);
                        infuseramptime = getnumber(5, command);
                    }
                    else if (command.Equals("wramp"))
                    {
                        reply = "wramp=" + refillstartrate.ToString("F2") + ", " + refillendrate.ToString("F2") + " ml/min " + refillramptime.ToString("F2") + " sec\n";
                    }
                    else if (command.IndexOf("wramp") != -1)
                    {
                        reply = ">\n";
                        refillstartrate = getnumber(1, command);
                        refillendrate = getnumber(2, command);
                        refillramptime = getnumber(4, command);
                    } 
                    else if ((command.Equals("cmd ultra")) ||
                             (command.Equals("poll remote")) ||
                             (command.IndexOf("tvolume") != -1) ||
                             (command.Equals("irun")) ||
                             (command.Equals("rrun")) ||
                             (command.IndexOf("diameter") != -1) ||
                             (command.IndexOf("gang") != -1))
                    {
                        reply = ">\n";
                    }
                    else
                    {
                        reply = "Command error:\r\n   Unknown Command = " + command + "\r\n>\n";
                    }
                    Thread.Sleep(100);
                    simulator.SendReply(reply);
                    Console.WriteLine("Reply = " + reply);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Console.ReadLine();
        }

        static double getnumber(int index, string command)
        {
            int index0 = index;
            double number = 0.0;
            int index1 = command.IndexOf(' ');
            string subStr = command;
            if (index1 > -1)
            {
                while (index0 > 0)
                {
                    index0--;
                    subStr = subStr.Substring(index1 + 1);
                    if (index0 == 0)
                    {
                        if (index < 4)
                            index1 = subStr.IndexOf(' ');
                        else
                            index1 = 0;
                    }
                    else
                        index1 = subStr.IndexOf(' ');
                }
                if (index1 > -1)
                {
                    if (index < 4)
                        subStr = subStr.Substring(0, index1);
                    number = Convert.ToDouble(subStr);
                }
            }
            return number;
        }
    }
}
