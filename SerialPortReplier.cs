﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;

namespace SyringPumpSimulator
{
    public class SerialPortReplier
    {
        private SerialPort _port = null;
        private string _command = "";
        private Semaphore _sem = null;
         
        public SerialPortReplier()
        {

        }

        private void Receive(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            _command = _port.ReadLine();
            _sem.Release();
        }

        public void OpenPort()
        {
            try
            {
                _port = new SerialPort();
                _port.PortName = "COM5";
                _port.BaudRate = 19200;
                _port.Handshake = System.IO.Ports.Handshake.None;
                _port.Parity = Parity.None;
                _port.DataBits = 8;
                _port.StopBits = StopBits.One;
                _port.ReadTimeout = 200;
                _port.WriteTimeout = 20;
                _port.Open();
                _sem = new Semaphore(0, 1);
                _port.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(Receive);
                Console.WriteLine("Simulator Port Opened");
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to open Simulator Port\n" + e);
                throw e;
            }
        }

        public string GetCommand()
        {
            if (_port.IsOpen)
            {
                _sem.WaitOne();
                string command = _command;
                _command = "";
                return command;
            }
            else
            {
                throw new Exception("Simulator Port is not opened");
            }
        }

        public void SendReply(string reply)
        {
            if (_port.IsOpen)
            {
                try
                {
                    byte[] hexstring = Encoding.ASCII.GetBytes(reply);
                    foreach (byte hexval in hexstring)
                    {
                        byte[] _hexval = new byte[] { hexval }; 
                        _port.Write(_hexval, 0, 1);
                        Thread.Sleep(1);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed to send reply\n" + e);
                    throw e;
                }
            }
            else
            {
                throw new Exception("Simulator Port is not opened");
            }
        }
    }
}
